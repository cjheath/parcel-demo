### Parcel Init code
This is the complete setup for a basic JS project which compiles SCSS and should just work as is.

Added to save me the time to figure this out again!

Run using:
```
parcel src/index.html
```

Watch files only using:
```
parcel watch src/index.html
```

Build prod versions using:
```
parcel build src/index.html
```